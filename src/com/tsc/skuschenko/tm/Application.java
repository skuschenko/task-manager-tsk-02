package com.tsc.skuschenko.tm;

import com.tsc.skuschenko.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        System.out.println("***Welcome to task manager***");
        parseArgs(args);

    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length<1 ) return;
        final String arg= args[0];
        if (TerminalConst.TM_ABOUT.toUpperCase().equals(arg.toUpperCase()) ) showAbout();
        else if (TerminalConst.TM_VERSION.toUpperCase().equals(arg.toUpperCase()) ) showVersion();
        else if (TerminalConst.TM_HELP.toUpperCase().equals(arg.toUpperCase()) ) showHelp();
        else System.out.println(arg+" command doesn't  found. "+
                    "Pleas print '"+TerminalConst.TM_HELP+"' for more information");
    }

    public static void showAbout() {
        System.out.println("["+ TerminalConst.TM_ABOUT.toUpperCase() +"]");
        System.out.println("AUTHOR: Semyon Kuschenko");
        System.out.println("EMAIL: skushchenko@tsconsulting.com");
    }

    public static void showVersion() {
        System.out.println("["+ TerminalConst.TM_VERSION.toUpperCase() +"]");
        System.out.println("1.0.0");
    }
    
    public static void showHelp() {
        System.out.println("["+ TerminalConst.TM_HELP.toUpperCase() +"]");
        System.out.println(TerminalConst.TM_HELP+" - show help information");
        System.out.println(TerminalConst.TM_VERSION+" - show version application");
        System.out.println(TerminalConst.TM_ABOUT+" - show about information");
    }

}
